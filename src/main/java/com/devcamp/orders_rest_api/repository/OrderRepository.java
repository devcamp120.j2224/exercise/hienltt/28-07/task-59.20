package com.devcamp.orders_rest_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.orders_rest_api.model.Orders;

public interface OrderRepository extends JpaRepository<Orders, Long> {
    
}
